# - Find ConTeXt
# This module finds if ConTeXt is installed and determines where the
# executables are. This code sets the following variables:
#
# CONTEXT_FOUND:      whether context has been found or not
# CONTEXT_EXECUTABLE: full path to the context executable if it has been found
# CONTEXT_VERSION:    version number of the available context
# LUATEX_FOUND:       whether luatex has been found or not
# LUATEX_EXECUTABLE:  full path to the luatex executable if it has been found
# LUATEX_VERSION:     version number of the available luatex

find_program(CONTEXT_EXECUTABLE
  NAMES context)
find_program(LUATEX_EXECUTABLE
  NAMES luatex)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(CONTEXT DEFAULT_MSG CONTEXT_EXECUTABLE)
find_package_handle_standard_args(LUATEX DEFAULT_MSG LUATEX_EXECUTABLE)

mark_as_advanced(CONTEXT_EXECUTABLE)
mark_as_advanced(LUATEX_EXECUTABLE)

if(CONTEXT_FOUND)
    execute_process(COMMAND ${CONTEXT_EXECUTABLE} "--version"
                    OUTPUT_VARIABLE "CONTEXT_VERSION")
    string(REGEX REPLACE ".*current version: ([^\r?\n? ?]*)[ ?].*" "\\1" "CONTEXT_VERSION" ${CONTEXT_VERSION})
    string(STRIP ${CONTEXT_VERSION} "CONTEXT_VERSION")
else(CONTEXT_FOUND)
    message(FATAL_ERROR "ConTeXt executable not found!")
endif(CONTEXT_FOUND)

if(LUATEX_FOUND)
    execute_process(COMMAND ${LUATEX_EXECUTABLE} "--version"
                    OUTPUT_VARIABLE "LUATEX_VERSION")
    string(REGEX REPLACE "This is LuaTeX, Version [^0-9]*([^\r?\n? ?]*)[\\-].*" "\\1" "LUATEX_VERSION" ${LUATEX_VERSION})
    string(STRIP ${LUATEX_VERSION} "LUATEX_VERSION")
else(LUATEX_FOUND)
    message(FATAL_ERROR "LuaTeX executable not found!")
endif(LUATEX_FOUND)

include(ConTeXtVersion)
include(LuaTeXVersion)
include(ConTeXtCompile)
