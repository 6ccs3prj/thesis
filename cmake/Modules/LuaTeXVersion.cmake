# This macro determines the available version of the executable. The following
# options are valid:
#
# EXACT:   needs to be available in the exact version given
# MINIMUM: needs to be available in the given version or any higher (default)
# MAXIMUM: needs to be available in the given version or any older than this

include(ParseArguments)

macro(luatex_minimum_required prefix version)
    parse_arguments(ARGS "" "MINIMUM;MAXIMUM;EXACT" ${ARGN})
        set(compare_message "")
        set(error_message "")
        if(ARGS_MINIMUM)
                set(compare_message "a minimum ")
                set(error_message "or greater ")
        elseif(ARGS_MAXIMUM)
                set(compare_message "a maximum ")
                set(error_message "or less ")
        endif(ARGS_MINIMUM)

        message(STATUS "checking for ${compare_message}LuaTeX version of ${version}")

        unset(version_accepted)
        # MINIMUM is the default if no option is specified
        if(ARGS_EXACT)
                if(${LUATEX_VERSION} VERSION_EQUAL ${version})
                        set(version_accepted TRUE)
                endif(${LUATEX_VERSION} VERSION_EQUAL ${version})
        elseif(ARGS_MAXIMUM)
                if(${LUATEX_VERSION} VERSION_LESS ${version} OR ${LUATEX_VERSION} VERSION_EQUAL ${version})
                        set(version_accepted TRUE)
                endif(${LUATEX_VERSION} VERSION_LESS ${version} OR ${LUATEX_VERSION} VERSION_EQUAL ${version})
        else(ARGS_MAXIMUM)
                if(${LUATEX_VERSION} VERSION_GREATER ${version} OR ${LUATEX_VERSION} VERSION_EQUAL ${version})
                        set(version_accepted TRUE)
                endif(${LUATEX_VERSION} VERSION_GREATER ${version} OR ${LUATEX_VERSION} VERSION_EQUAL ${version})
        endif(ARGS_EXACT)

        if (NOT version_accepted)
                message(FATAL_ERROR "LuaTeX version ${version} ${error_message}is required.")
        endif(NOT version_accepted)

        message(STATUS "  found LuaTeX, version ${LUATEX_VERSION}")
endmacro(luatex_minimum_required)
