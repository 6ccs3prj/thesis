# This macro invokes context and performs linearization. The following options
# are valid:
#
# SOURCE:  a list of tex source files
# OPTIONS: additional context options

include(ParseArguments)
find_package(QPDF)

macro(context_project output)
    parse_arguments(ARGS "SOURCE;OPTIONS;DIRECTORY" "" ${ARGN})
    if(ARGS_DIRECTORY)
        set(CTX_TEX_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${ARGS_DIRECTORY})
    else(ARGS_DIRECTORY)
        set(CTX_TEX_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR})
    endif(ARGS_DIRECTORY)

    set(in_files "")
    set(out_files "")
    set(out_files_base "")
    set(${output} "")
    foreach(src ${ARGS_DEFAULT_ARGS})
        list(APPEND in_files "${CMAKE_CURRENT_SOURCE_DIR}/${src}")
        set(out_file "${CTX_TEX_SOURCE_DIR}/${src}")
        list(APPEND ${output} ${out_file})
        string(REPLACE ".tex" "" src ${src})
        list(APPEND out_files "${CMAKE_CURRENT_BINARY_DIR}/${src}")
        list(APPEND out_files_base "${src}")
    endforeach(src ${ARGS_DEFAULT_ARGS})

    file(GLOB BIBTEX_FILES ${CTX_TEX_SOURCE_DIR}/*.bib)
    file(COPY ${BIBTEX_FILES} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
    foreach(ABSOLUTE_BIBTEX_FILES ${BIBTEX_FILES})
        string(REPLACE "${CTX_TEX_SOURCE_DIR}/" "" RELATIVE_BIBTEX_FILES ${ABSOLUTE_BIBTEX_FILES})
        set(binary_dir_bib "${CMAKE_BINARY_DIR}/${RELATIVE_BIBTEX_FILES}")
        list(APPEND make_clean_bib "${binary_dir_bib}")
    endforeach(ABSOLUTE_BIBTEX_FILES ${BIBTEX_FILES})
    list(APPEND make_clean "${make_clean_bib}")

    set(CTX_FILES)
    foreach(ctx ${out_files_base})
    add_custom_command(
    OUTPUT
        ${CMAKE_CURRENT_BINARY_DIR}/${ctx}.pdf
    COMMAND
        ${CONTEXT_EXECUTABLE}
    ARGS
        "--batchmode"
        "--noconsole"
        "--path=${CTX_TEX_SOURCE_DIR},${CTX_TEX_SOURCE_DIR}/graphics"
        "--silent"
        ${ARGS_OPTIONS}
        ${CTX_TEX_SOURCE_DIR}/${ctx}.tex
    DEPENDS
        ${CTX_TEX_SOURCE_DIR}/${ctx}.tex
    COMMENT
        "${ctx}.pdf"
    )
    list(APPEND CTX_FILES ${CMAKE_CURRENT_BINARY_DIR}/${ctx}.pdf)
    endforeach(ctx ${out_files_base})
    add_custom_target("ConTeXt" ALL DEPENDS ${CTX_FILES})

    # compile targets for all input files
    file(GLOB texfiles "${CTX_TEX_SOURCE_DIR}/*.tex")
    foreach(texinput ${texfiles})
    string(REPLACE "${CTX_TEX_SOURCE_DIR}/" "" texinput ${texinput})
    string(REPLACE ".tex" "" texinput ${texinput})
    add_custom_command(
    OUTPUT
        ${CMAKE_CURRENT_BINARY_DIR}/${texinput}.pdf
    COMMAND
        ${CONTEXT_EXECUTABLE}
    ARGS
        "--batchmode"
        "--noconsole"
        "--path=${CTX_TEX_SOURCE_DIR},${CTX_TEX_SOURCE_DIR}/graphics"
        "--silent"
        ${ARGS_OPTIONS}
        ${CTX_TEX_SOURCE_DIR}/${texinput}.tex
    DEPENDS
        ${CTX_TEX_SOURCE_DIR}/${texinput}.tex
    COMMENT
        "${texinput}.pdf"
    )
    add_custom_target("${texinput}.tex" DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${texinput}.pdf)
    endforeach(texinput ${texfiles})

    if(QPDF_FOUND)
    set(QPDF_FILES)
    foreach(qpdf ${out_files_base})
        add_custom_command(
        OUTPUT
            ${CMAKE_CURRENT_BINARY_DIR}/${qpdf}-web.pdf
        COMMAND
            ${QPDF_EXECUTABLE}
        ARGS
            "--linearize"
            ${CMAKE_CURRENT_BINARY_DIR}/${qpdf}.pdf
            ${CMAKE_CURRENT_BINARY_DIR}/${qpdf}-web.pdf
        DEPENDS
            ${CMAKE_CURRENT_BINARY_DIR}/${qpdf}.pdf
        COMMENT
            "${qpdf}-web.pdf"
        )
    list(APPEND QPDF_FILES ${CMAKE_CURRENT_BINARY_DIR}/${qpdf}-web.pdf)
    endforeach(qpdf ${out_files_base})
    add_custom_target("QPDF" ALL DEPENDS ${QPDF_FILES})
    endif(QPDF_FOUND)
endmacro(context_project)
